package com.example.stocksApp.repositories;

import com.example.stocksApp.models.User;
import org.springframework.data.repository.CrudRepository;
public interface UserRepository extends CrudRepository<User,Long> {
}
