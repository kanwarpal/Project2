package com.example.stocksApp.models;

import lombok.*;
import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name= "USERS")
public class User {

@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "QUOTE")
    private String quote;

    @Column(name = "LTP")
    private String ltp;

    @Column(name = "DAYS_HIGH")
    private String daysHigh;

    @Column(name = "DAYS_LOW")
    private String daysLow;

    @Column(name = "CAP")
    private String cap;

    public User(String quote, String ltp, String daysHigh, String daysLow, String cap){
        this.quote=quote;
        this.ltp=ltp;
        this.daysHigh=daysHigh;
        this.daysLow=daysLow;
        this.cap=cap;

    }

    public Long getId() {
        return id;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }
}
